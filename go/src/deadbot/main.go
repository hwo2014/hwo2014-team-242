package main

import (
	"bufio"
	"deadbot/bot"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"strconv"
)

func connect(host string, port int) (conn net.Conn, err error) {
	conn, err = net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	return
}

type GameMessage struct {
	MsgType string          `json:"msgType"`
	Data    json.RawMessage `json:"data"`
}

func readMsg(reader *bufio.Reader) (msg GameMessage, err error) {
	var line string
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}

	var gameMessage GameMessage
	err = json.Unmarshal([]byte(line), &gameMessage)
	if err != nil {
		return
	}
	return
}

func write(writer *bufio.Writer, data []byte) (err error) {
	//log.Println(string(data))
	_, err = writer.Write(data)
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func writeMsg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	write(writer, []byte(payload))
	if err != nil {
		return
	}
	return
}

func send_ping(writer *bufio.Writer) (err error) {
	err = writeMsg(writer, "ping", make(map[string]string))
	return
}

func send_throttle(writer *bufio.Writer, throttle float64) (err error) {
	err = writeMsg(writer, "throttle", throttle)
	return
}

func send_switchlane(writer *bufio.Writer, lane string) (err error) {
	err = writeMsg(writer, "switchLane", lane)
	log.Println("Switchlane")
	return
}

func send_turbo(writer *bufio.Writer) (err error) {
	err = writeMsg(writer, "turbo", "Vroom!")
	log.Println("TURBO!")
	return
}

/*
{"msgType": "joinRace", "data": {
  "botId": {
    "name": "keke",
    "key": "IVMNERKWEW"
  },
  "trackName": "hockenheim",
  "password": "schumi4ever",
  "carCount": 3
}}
*/
type JoinRace struct {
	MsgType string `json:"msgType"`
	Data    struct {
		//	BotId struct {
		Name string `json:"name"`
		Key  string `json:"key"`
	} `json:"data"`
	TrackName string `json:"trackName,omitempty"`
	Password  string `json:"password,omitempty"`
	CarCount  int    `json:"carCount,omitempty"`
	//} `json:"data"`
}

type YourCar struct {
	Name  string `json:"name"`
	Color string `json:"color"`
}

func main() {
	// Parse arguments
	args := os.Args[1:]
	if len(args) != 4 {
		log.Fatal("Usage: ./run host port botname botkey")
	}
	host := args[0]
	port, err := strconv.Atoi(args[1])
	if err != nil {
		log.Fatalf("Could not parse port value to integer: %v\n", args[1])
	}
	name := args[2]
	key := args[3]
	log.Println("Got arguments:", host, port, name, key)

	// Connect to server
	conn, err := connect(host, port)
	if err != nil {
		log.Fatalln("Error while connecting:", err)
	}
	defer conn.Close()

	bot := bot.NewBot()
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)

	join := JoinRace{}
	join.MsgType = "join"
	join.Data.Name = name
	join.Data.Key = key
	//join.Data.BotId.Name = name
	//join.Data.BotId.Key = key
	//join.Data.TrackName = "germany"
	//join.Data.Password = "peruna"
	//join.Data.CarCount = 1

	payload, err := json.Marshal(join)
	log.Println(string(payload))
	write(writer, payload)
	if err != nil {
		log.Fatalln(err)
	}

	// Main loop
	for {
		line, err := reader.ReadBytes('\n')
		if err != nil {
			log.Fatalln(err)
		}
		//log.Println(string(line))
		var gameMessage GameMessage
		err = json.Unmarshal(line, &gameMessage)
		if err != nil {
			log.Fatalln(err)
		}

		switch gameMessage.MsgType {
		case "join":
			bot.SetJoin(gameMessage.Data)
			send_ping(writer)
		case "yourCar":
			bot.SetYourCar("DeadCoffee", "red")
		case "gameInit":
			bot.SetGameInit(gameMessage.Data)
		case "gameStart":
			bot.SetGameStart(gameMessage.Data)
			send_ping(writer)
		case "crash":
			bot.SetCrash(gameMessage.Data)
			send_ping(writer)
		case "gameEnd":
			bot.SetGameEnd(gameMessage.Data)
			//os.Exit(0)
		case "carPositions":
			bot.SetCarPositions(gameMessage.Data)
			if sl := bot.GetSwitchLane(); sl != "" {
				send_switchlane(writer, sl)
			} else if bot.GetTurbo() {
				send_turbo(writer)
			} else {
				send_throttle(writer, bot.GetThrottle())
			}
		case "lapFinished":
			bot.SetLapFinished(gameMessage.Data)
			send_ping(writer)
		case "error":
			log.Printf(fmt.Sprintf("Got error: %v", gameMessage))
			send_ping(writer)
		case "turboAvailable":
			bot.SetTurboAvailable()
			send_ping(writer)
		default:
			log.Printf("Got msg type: %s", gameMessage.MsgType)
			send_ping(writer)
			log.Println(string(line))
		}
		//log.Println(string(line))
	}

}
