package bot

import (
	"encoding/json"
	"log"
	"math"
)

type Bot struct {
	Name         string
	Color        string
	MyCar        *Car
	GameInfo     GameInfo
	Throttle     float64
	GameTick     int
	DesiredSpeed float64
	DesiredLane  string
	Turbo        bool
}

func NewBot() Bot {
	b := &Bot{}
	b.DesiredSpeed = 6.6
	b.DesiredLane = "xxx"
	b.Turbo = false
	return *b
}

func (b *Bot) SetTurboAvailable() {
	b.Turbo = true
}

func (b *Bot) GetTurbo() bool {
	carPiece := b.GameInfo.GetPiece(b.MyCar.PiecePosition.PieceIndex)
	longestPiece := b.GameInfo.GetLongestStraight()
	if b.Turbo && carPiece == longestPiece {
		b.Turbo = false
		return true
	}
	return false
}

func (b *Bot) GetSwitchLane() string {
	if !b.GameInfo.GetPiece(b.MyCar.PiecePosition.PieceIndex).Switch {
		b.DesiredLane = ""
		return ""
	}
	angle := b.GameInfo.AngleBetweenNextTwoSwitches(b.MyCar)
	dir := ""
	if angle > 0 {
		dir = "Right"
	} else if angle < 0 {
		dir = "Left"
	}
	if b.DesiredLane != dir {
		b.DesiredLane = dir
		return b.DesiredLane
	}
	return ""
}

func (b *Bot) GetThrottle() float64 {

	// MAGIC NUMBERS
	magicCurve := 0.4200 // safe 0.4500 best 0.465
	magicBrake := 0.0210 // safe 0.0320 best 0.033
	// -------------

	dist := b.GameInfo.DistToNextCurve(b.MyCar)
	angle := math.Abs(b.MyCar.Angle)
	speed := b.GameInfo.GetSpeed(b.MyCar)
	curve := b.GameInfo.GetNextCurve(b.MyCar)
	curveAngle := b.GameInfo.AngleBetweenNextTwoSwitches(b.MyCar)
	f := float64(curve.Radius) * math.Pow(speed, 2)
	v := math.Sqrt(float64(b.GameInfo.GetNextCurveRadius(b.MyCar)) * magicCurve)
	b.DesiredSpeed = v + dist*magicBrake

	if speed < b.DesiredSpeed {
		b.Throttle = 1
	} else {
		b.Throttle = 0
	}
	log.Printf("THR[%.1f] SPD[%.2f] ANG[%.2f] DIST[%.2f] FORCE[%.2f] NCS[%.2f] DSPD[%.2f] CANG[%.2f]",
		b.Throttle, speed, angle, dist, f, v, b.DesiredSpeed, curveAngle)
	return b.Throttle
}

func (b *Bot) GetCar() *Car {
	return b.GameInfo.GetCarByName(b.MyCar.Id.Name)
}

func (b *Bot) SetJoin(data []byte) {
}

func (b *Bot) SetYourCar(name string, color string) {
	b.Name = name
	b.Color = color
}

func (b *Bot) SetGameInit(data []byte) {
	gi := GameInfo{}
	err := json.Unmarshal(data, &gi)
	if err != nil {
		log.Fatalln(err)
	}
	b.GameInfo = gi
}

func (b *Bot) SetGameStart(data []byte) {
}

func (b *Bot) SetCrash(data []byte) {
	log.Println("Someone Crashed!")
	log.Println(string(data))
}

func (b *Bot) SetCarPositions(data []byte) {
	cp := []*Car{}
	err := json.Unmarshal(data, &cp)
	if err != nil {
		log.Fatalln(err)
	}

	for _, e := range cp {
		t := b.GameInfo.GetCarByName(e.Id.Name)
		e.LastPiecePosition = t.PiecePosition
	}

	b.GameInfo.Race.Cars = cp
	b.MyCar = b.GameInfo.GetCarByName(b.Name)
}

func (b *Bot) SetGameEnd(data []byte) {
}

func (b *Bot) SetLapFinished(data []byte) {
	log.Println(string(data))
}
