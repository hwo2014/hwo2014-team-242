package bot

type Car struct {
	Id struct {
		Name  string `json:"name"`
		Color string `json:"color"`
	} `json:"id"`
	Dimensions struct {
		Length            float64 `json:"length"`
		Width             float64 `json:"width"`
		GuideFlagPosition float64 `json:"guideFlagPosition"`
	} `json:"dimensions"`
	Angle             float64       `json:"angle"`
	PiecePosition     PiecePosition `json:"piecePosition"`
	LastPiecePosition PiecePosition
}

type PiecePosition struct {
	PieceIndex      int     `json:"pieceIndex"`
	InPieceDistance float64 `json:"inPieceDistance"`
	Lane            struct {
		StartLaneIndex int `json:"startLaneIndex"`
		EndLaneIndex   int `json:"EndLaneIndex"`
	} `json:"lane"`
	Lap int `json:"lap"`
}
