package bot

import (
	"log"
	"math"
)

func (g *GameInfo) GetNextCurveRadius(c *Car) int {
	for i := 0; i < len(g.Race.Track.Pieces); i++ {
		p := g.GetPiece(c.PiecePosition.PieceIndex + i)
		if p.Angle > 0 {
			return p.Radius - g.Race.Track.Lanes[c.PiecePosition.Lane.StartLaneIndex].DistanceFromCenter
		} else if p.Angle < 0 {
			return p.Radius + g.Race.Track.Lanes[c.PiecePosition.Lane.StartLaneIndex].DistanceFromCenter
		}
	}
	return 0
}

func (g *GameInfo) AngleBetweenNextTwoSwitches(c *Car) float64 {
	var angle float64 = 0
	var skip bool = true
	for i := 0; i < len(g.Race.Track.Pieces); i++ {
		p := g.GetPiece(c.PiecePosition.PieceIndex + i)
		//log.Println(p.Angle, p.Radius)
		if !skip {
			if p.Switch {
				return angle
			}
			angle += p.Angle
		}
		if p.Switch && skip {
			skip = false
		}
	}
	return angle
}
func (g *GameInfo) GetLongestStraight() *Piece {
	var longestPiece *Piece
	var longestDist float64 = 0
	var countingPiece *Piece
	var countingDist float64 = 0

	var counting bool = false
	var startFrom int = 0

	for i := 0; i < len(g.Race.Track.Pieces); i++ {
		p := g.GetPiece(i)
		if !counting && p.Angle != 0 {
			counting = true
		}
		if counting && p.Angle == 0 {
			startFrom = i
			counting = false
			break
		}
	}

	for i := 0; i < len(g.Race.Track.Pieces); i++ {
		p := g.GetPiece(i + startFrom)
		if !counting && p.Angle == 0 {
			counting = true
			countingPiece = p
			countingDist = 0
		}
		if counting && p.Angle == 0 {
			countingDist += p.Length
		}
		if counting && p.Angle != 0 {
			counting = false
			if countingDist > longestDist {
				longestDist = countingDist
				longestPiece = countingPiece
			}
		}
	}
	return longestPiece
}

func (g *GameInfo) GetNextCurve(c *Car) *Piece {
	for i := 0; i < len(g.Race.Track.Pieces); i++ {
		p := g.GetPiece(c.PiecePosition.PieceIndex + i)
		if p.Angle != 0 {
			return p
		}
	}
	return &Piece{}
}

func (g *GameInfo) DistToNextCurve(c *Car) float64 {
	var dist float64 = 0
	if g.GetPiece(c.PiecePosition.PieceIndex).Angle != 0 {
		return 0
	}
	for i := 1; i < len(g.Race.Track.Pieces); i++ {
		p := g.GetPiece(c.PiecePosition.PieceIndex + i)
		if p.Angle != 0 {
			dist += g.GetPieceDistLeft(c)
			return dist
		}
		dist += p.Length
	}
	return dist
}

func (g *GameInfo) DistToNextStraight(c *Car) float64 {
	return 0
}

func (g *GameInfo) GetPiece(i int) *Piece {
	p := g.Race.Track.Pieces
	return p[i%len(p)]
}

func (g *GameInfo) GetPieceDistLeft(c *Car) float64 {
	p := g.GetPiece(c.PiecePosition.PieceIndex)
	ang := p.Angle
	if ang != 0 {
		rad := p.Radius
		laneDist := g.GetLane(c.PiecePosition.Lane.EndLaneIndex).DistanceFromCenter
		if ang > 0 {
			rad -= laneDist
		} else {
			rad += laneDist
		}
		dist := float64(rad) * math.Pi * (math.Abs(ang) / 360) * 2
		return dist - c.PiecePosition.InPieceDistance
	} else {
		dist := g.GetPiece(c.PiecePosition.PieceIndex).Length
		dist -= c.PiecePosition.InPieceDistance
		return dist
	}
}

func (g *GameInfo) GetSpeed(c *Car) float64 {
	if c.LastPiecePosition.PieceIndex != c.PiecePosition.PieceIndex {
		ang := g.GetPiece(c.LastPiecePosition.PieceIndex).Angle
		if ang != 0 {
			rad := g.GetPiece(c.LastPiecePosition.PieceIndex).Radius
			//pieceLength := float64(rad) * math.Pi * (math.Abs(ang) / 360) * 2
			if ang > 0 {
				rad -= g.GetLane(c.LastPiecePosition.Lane.EndLaneIndex).DistanceFromCenter
			} else {
				rad += g.GetLane(c.LastPiecePosition.Lane.EndLaneIndex).DistanceFromCenter
			}
			dist := float64(rad) * math.Pi * (math.Abs(ang) / 360) * 2

			//log.Println("Last piece length:", pieceLength, " Lane:", dist)

			dist -= c.LastPiecePosition.InPieceDistance
			//log.Println("Last inpieceDistance:", c.LastPiecePosition.InPieceDistance)
			return c.PiecePosition.InPieceDistance + dist
		} else {
			dist := g.GetPiece(c.LastPiecePosition.PieceIndex).Length
			dist -= c.LastPiecePosition.InPieceDistance
			return c.PiecePosition.InPieceDistance + dist
		}
	}
	return c.PiecePosition.InPieceDistance - c.LastPiecePosition.InPieceDistance
}

// Return a pointer to a car with name, or empty struct if not found
func (g *GameInfo) GetCarByName(name string) *Car {
	for _, element := range g.Race.Cars {
		if element.Id.Name == name {
			return element
		}
	}
	log.Println("Can't find car", name)
	return &Car{}
}

// Return a pointer to a car with name, or empty struct if not found
func (g *GameInfo) GetLane(index int) *Lane {
	for _, element := range g.Race.Track.Lanes {
		if element.Index == index {
			return element
		}
	}
	log.Println("Can't find lane", index)
	return &Lane{}
}

type Piece struct {
	Radius int     `json:"radius"`
	Length float64 `json:"length"`
	Switch bool    `json:"switch"`
	Angle  float64 `json:"angle"`
}

type Track struct {
	Id            string   `json:"id"`
	Name          string   `json:"name"`
	Pieces        []*Piece `json:"pieces"`
	Lanes         []*Lane  `json:"lanes"`
	StartingPoint struct {
		Position struct {
			X float64 `json:"x"`
			Y float64 `json:"y"`
		}
		Angle float64 `json:"angle"`
	} `json:"startingPoint"`
}

type Lane struct {
	DistanceFromCenter int `json:"distanceFromCenter"`
	Index              int `json:"index"`
}

type Session struct {
	Laps         int  `json:"laps"`
	MaxLapTimeMs int  `json:"maxLapTimeMs"`
	QuickRace    bool `json:"quickRace"`
}

type GameInfo struct {
	Race struct {
		Track   Track   `json:"track"`
		Cars    []*Car  `json:"cars"`
		Session Session `json:"raceSession"`
	} `json:"race"`
}
